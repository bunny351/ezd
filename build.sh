#!/bin/sh

# libsigsegv-dev and libX11-dev packages are required.

tar xfz scheme2c-69014dc.tar.gz
cd scheme2c-69014dc
make forLINUX
cd LINUX
make prefix=`pwd`/../install-root port
make prefix=`pwd`/../install-root install
cd ../cdecl
make SCC=../install-root/bin/scc all
cd ../xlib
make SCC=../install-root/bin/scc XLIB=-lX11 RANLIB=ranlib all
cd ../..

cd src
PATH=../scheme2c-69014dc/install-root/bin:$PATH make ezd X11=-lX11 SCXL=../scheme2c-69014dc/xlib/scxl.a SC=../scheme2c-69014dc/install-root/lib/scheme2c/libsc.a

# now copy ezd to some place in your PATH

